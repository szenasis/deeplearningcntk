﻿using System;
using System.Collections.Generic;
using System.IO;
using CNTK;
using System.Linq;

namespace SemiNeuralNetwork
{
    class SemiNeuralNetwork
    {
        const int inputSize = 4;
        const int hiddenNeuronCount = 3;
        const int outputSize = 1;

        readonly Variable x;
        readonly Function y;
        readonly Parameter w1, b, w2;

        public SemiNeuralNetwork()
        {
            // Build graph
            x = Variable.InputVariable(new int[] { inputSize, 1  }, DataType.Float);
            w1 = new Parameter(new int[] { hiddenNeuronCount, inputSize }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            b = new Parameter(new int[] { hiddenNeuronCount, 1 }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            w2 = new Parameter(new int[] { outputSize, hiddenNeuronCount }, DataType.Float, CNTKLib.GlorotNormalInitializer());
            y = CNTKLib.Times(w2, CNTKLib.Plus(CNTKLib.Times(w1, x), b));
        }

        public void Train(string[] trainData)
        {
            int n = trainData.Length;

            // Extend graph
            Variable yt = Variable.InputVariable(new int[] { outputSize, 1 }, DataType.Float);
            Function sqDiff = CNTKLib.Square(CNTKLib.Minus(y, yt));
            Function loss = CNTKLib.ReduceSum(sqDiff, Axis.AllAxes());
            Learner learner = CNTKLib.SGDLearner(new ParameterVector() { w1, b, w2 }, new TrainingParameterScheduleDouble(0.001, 1));
            Trainer trainer = Trainer.CreateTrainer(loss, loss, null, new List<Learner>() { learner });

            // Train
            for (int i = 1; i <= 100; i++)
            {
                double sumLoss = 0;
                foreach (string line in trainData)
                {
                    float[] values = line.Split('\t').Select(x => float.Parse(x)).ToArray();
                    var inputDataMap = new Dictionary<Variable, Value>()
                    {
                        { x, LoadInput(values[0], values[1], values[2], values[3]) },
                        { yt, Value.CreateBatch(yt.Shape, new float[] { values[4] }, DeviceDescriptor.CPUDevice) }
                    };
                    var outputDataMap = new Dictionary<Variable, Value>() { { loss, null } };

                    trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
                    loss.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
                    sumLoss += outputDataMap[loss].GetDenseData<float>(loss)[0][0];
                }
                Console.WriteLine(String.Format("{0}\tloss:{1}", i, sumLoss / n));
            }
        }

        public float Prediction(float age, float height, float weight, float salary)
        {
            var inputDataMap = new Dictionary<Variable, Value>() { { x, LoadInput(age, height, weight, salary) } };
            var outputDataMap = new Dictionary<Variable, Value>() { { y, null } };
            y.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
            return outputDataMap[y].GetDenseData<float>(y)[0][0];
        }

        Value LoadInput(float age, float height, float weight, float salary)
        {
            float[] x_store = new float[inputSize];
            x_store[0] = age / 100;
            x_store[1] = height / 250;
            x_store[2] = weight / 150;
            x_store[3] = salary / 1500000;
            return Value.CreateBatch(x.Shape, x_store, DeviceDescriptor.CPUDevice);
        }
    }

    public class Program
    {
        string[] trainData = File.ReadAllLines(@"..\data\HusbandEvaluation.txt");
        SemiNeuralNetwork app = new SemiNeuralNetwork();

        void Run()
        {
            app.Train(trainData);
            FileTest();
            ConsoleTest();
        }

        void FileTest()
        {
            int goodPrediction = 0, wrongPrediction = 0;
            foreach (string line in trainData)
            {
                float[] values = line.Split('\t').Select(x => float.Parse(x)).ToArray();
                float pred = app.Prediction(values[0], values[1], values[2], values[3]);

                if (Math.Round(pred) != values[4])
                {
                    Console.WriteLine("--- " + pred + "\t" + line);
                    wrongPrediction++;
                }
                else
                {
                    Console.WriteLine("+++ " + pred + "\t" + line);
                    goodPrediction++;
                }
            }
            Console.WriteLine(String.Format("Good prediction:{0} ({1}%)", goodPrediction, 100f * goodPrediction / trainData.Count()));
            Console.WriteLine(String.Format("Wrong prediction:{0} ({1}%)", wrongPrediction, 100f * wrongPrediction / trainData.Count()));
        }

        void ConsoleTest()
        {
            while (true)
            {
                Console.Write("Age:");
                float age = float.Parse(Console.ReadLine());
                Console.Write("Height:");
                float height = float.Parse(Console.ReadLine());
                Console.Write("Weight:");
                float weight = float.Parse(Console.ReadLine());
                Console.Write("Salary:");
                float salary = float.Parse(Console.ReadLine());

                Console.WriteLine("Prediction:"+ app.Prediction(age, height, weight, salary));
            }
        }

        static void Main(string[] args)
        {
            new Program().Run();
        }
    }
}
