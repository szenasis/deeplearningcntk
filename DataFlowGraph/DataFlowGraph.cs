﻿using System;
using System.Collections.Generic;
using CNTK;

namespace OE.Courses.DeepLearning
{
    class DataFlowGraph
    {
        void BuildAndTestDataFlowGraph()
        {
            // Build graph
            Variable a = Variable.InputVariable(new NDShape(1), DataType.Float, "a");
            Variable b = Variable.InputVariable(new NDShape(1), DataType.Float, "b");
            Function f = CNTKLib.Plus(a, b, "f");

            // Prepare data
            Value a_value = Value.CreateBatch(new NDShape(1,1), new List<float>() { 2.0f }, DeviceDescriptor.CPUDevice);
            Value b_value = Value.CreateBatch(new NDShape(1,1), new List<float>() { 3.0f }, DeviceDescriptor.CPUDevice);

            // Evaluate
            var inputDataMap = new Dictionary<Variable, Value>()
            {
                { a, a_value },
                { b, b_value }
            };

            var outputDataMap = new Dictionary<Variable, Value>()
            {
                { f, null }
            };

            f.Evaluate(inputDataMap, outputDataMap, DeviceDescriptor.CPUDevice);
            float result = outputDataMap[f].GetDenseData<float>(f)[0][0];
            Console.WriteLine(result);
        }

        static void Main(string[] args)
        {
            new DataFlowGraph().BuildAndTestDataFlowGraph();
        }
    }
}
