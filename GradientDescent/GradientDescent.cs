﻿using System;

namespace OE.Courses.DeepLearning
{
    class GradientDescent
    {
        const float a = 3.0f;
        const float rndX = 10.0f;
        const float learningRate = 0.2f;
        const int iterationCount = 30;

        // f(x) = (x-a)^2
        float f(float x)
        {
            return (x - a) * (x - a);
        }

        // df/dx(x) = 2(x-a)
        float df(float x)
        {
            return 2 * (x - a);
        }

        void RunGradientDescent()
        {
            float x = rndX;
            for(int i = 1; i <= iterationCount; i++)
            {
                float fx = f(x);
                float dfx = df(x);
                Console.WriteLine(String.Format("{0}\t{1}\t{2}\t{3}", i, x, fx, dfx));
                x -= learningRate * dfx;
            }
        }

        static void Main(string[] args)
        {
            new GradientDescent().RunGradientDescent();
        }
    }
}
