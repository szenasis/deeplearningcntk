﻿using System;
using System.Collections.Generic;
using CNTK;

namespace OE.Courses.DeepLearning
{
    class GradientDescentCNTK
    {
        const float aValue = 3.0f;
        const float rndX = 10.0f;
        const float learningRate = 0.2f;
        const int iterationCount = 30;

        void RunGradientDescent()
        {
            // Build graph
            // f(x) = (x-a)^2
            Variable a = Variable.InputVariable(new int[] { 1 }, DataType.Float);
            Parameter x = new Parameter(new int[] { 1 }, DataType.Float, rndX);
            Function f1 = CNTKLib.Minus(x, a);
            Function f = CNTKLib.Times(f1, f1);

            Learner learner = CNTKLib.SGDLearner(new ParameterVector() { x }, new TrainingParameterScheduleDouble(learningRate, 1));
            Trainer trainer = Trainer.CreateTrainer(f, f, null, new List<Learner>() { learner });

            // Prepare data
            Value a_value = Value.CreateBatch(new int[] { 1 }, new List<float>() { aValue }, DeviceDescriptor.CPUDevice);

            var inputDataMap = new Dictionary<Variable, Value>()
            {
                { a, a_value }
            };

            // Train
            for (int i = 1; i <= iterationCount; i++)
            {
                float xValue = new Value(x.GetValue()).GetDenseData<float>(x)[0][0];
                Console.WriteLine(String.Format("{0}\t{1}", i, xValue));
                trainer.TrainMinibatch(inputDataMap, false, DeviceDescriptor.CPUDevice);
            }
        }

        static void Main(string[] args)
        {
            new GradientDescentCNTK().RunGradientDescent();
        }
    }
}
