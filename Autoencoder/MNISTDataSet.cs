﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MNIST
{
    // file format: http://yann.lecun.com/exdb/mnist/
    public class MNISTDataSet
    {
        public int Width { get; set; }
        public int Height { get; set; }

        public const int InputSize = 28 * 28;
        public List<float> Input { get; set; } = new List<float>();

        public const int OutputSize = 10;
        public List<float> Output { get; set; } = new List<float>();

        public int Count { get; set; }

        public MNISTDataSet(string indexFilename, string imageFilename)
        {
            if (indexFilename != null)
                LoadIndex(indexFilename);
            if (imageFilename != null)
                LoadImage(imageFilename);
        }

        void LoadIndex(string filename)
        {
            byte[] data = File.ReadAllBytes(filename);
            Count = BitConverter.ToInt32(new byte[] { data[7], data[6], data[5], data[4] }, 0);
            for (int i = 0; i < Count; i++)
                for (int d = 0; d <= 9; d++)
                    Output.Add(d == data[8 + i] ? 1.0f : 0.0f);
        }

        void LoadImage(string filename)
        {
            byte[] data = File.ReadAllBytes(filename);
            Count = BitConverter.ToInt32(new byte[] { data[7], data[6], data[5], data[4] }, 0);
            Width = BitConverter.ToInt32(new byte[] { data[11], data[10], data[9], data[8] }, 0);
            Height = BitConverter.ToInt32(new byte[] { data[15], data[14], data[13], data[12] }, 0);
            for (int i = 0; i < Count * InputSize; i++)
                Input.Add(data[16 + i] / 255.0f);
        }

        public String DataToString(int index)
        {
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < InputSize; i++)
            {
                if (i % Width == 0)
                {
                    if (i / Width == Height / 2)
                    {
                        sb.Append("\t [");
                        for (int d = 0; d <= 9; d++)
                        {
                            if (d != 0) sb.Append(", ");
                            sb.Append(String.Format("{0:0.00}", Output[index * OutputSize + d]));
                        }
                        sb.Append("]");

                    }
                    sb.Append("\n");
                }
                float data = Input[index * InputSize + i];
                if (data > 0.8)
                    sb.Append("\u2593");
                else
                if (data > 0.6)
                    sb.Append("\u2592");
                else
                if (data > 0.4)
                    sb.Append("\u2591");
                else
                    sb.Append(" ");
            }
            return sb.ToString();
        }
    }
}
